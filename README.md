# Graphic Editor

This a sample code to draw coes. It was built for an interview with UrbanSports company.

As I love sports and its industry I tried my best to solve this.

The following is the details:

Graphic Editor
Please prepare a service for a simple app called “Graphic Editor”. The service has to be able to draw
geometric shapes (circle, square, rectangle, ellipse, etc). Add implementation for circle and square, but
there should be possibility to add any other shape in the service fast, easy and with minimum code
changes. Each shape might have various attributes: color, border size, etc.
The app usage: It gets a multi-array to the
input. Example:
$shapes = [
['type' => 'circle', 'params' => [...]],
['type' => 'circle', 'params' => [...]] ];
It makes all calculations and shows a result in any format (array of points, image(binary file) etc.).
Prepare an architecture for this service in PHP7 with controller/cli endpoint and upload it to github.
Important:
* No use of any frameworks or third-party code
* Please, skip all calculations for shapes (mocks, dummy methods are OK here). The architecture is more
interesting for us
* OOP design patterns are required

Good luck!


# How to test:
- Clone this repositroy
- Make sure php7 or greater is installed
- CD into the project
- use command: "php draw --type=circle --color=#989"

Feel free to use different params, type or even the main command which is draw :D


For adding another shape we just need to Add another class to
src/GraphicEditor/Shapes/ directory. The only function that we need is the logic of making the shape.
Then based on the name of the class you can use it in CLI to see the results.

Thnaks a lot in advance.