<?php

// load interfaces
foreach (glob(__DIR__ . "/src/GraphicEditor/Interfaces/*.php") as $filename) {
    include_once $filename;
}

// load formats
foreach (glob(__DIR__ . "/src/GraphicEditor/Formats/*.php") as $filename) {
    include_once $filename;
}

// load Exceptions
foreach (glob(__DIR__ . "/src/GraphicEditor/Exceptions/*.php") as $filename) {
    include_once $filename;
}

// load Shapes
foreach (glob(__DIR__ . "/src/GraphicEditor/Shapes/*.php") as $filename) {
    include_once $filename;
}

// load main lib
foreach (glob(__DIR__ . "/src/GraphicEditor/*.php") as $filename) {
    include_once $filename;
}