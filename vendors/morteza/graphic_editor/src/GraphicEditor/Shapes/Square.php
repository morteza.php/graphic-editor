<?php


namespace GraphicEditor\Shapes;


use GraphicEditor\Formats\BaseFormat;

class Square extends BaseShape
{
    /**
     * @param BaseFormat $baseFormat
     * @return array|mixed
     */
    public function draw(BaseFormat $baseFormat)
    {
        // here based on format we generate the results


        // return dummy result
        $shape = [
            'title' => 'I am supposed to be a Square',
            'color' => $this->color,
            'border_size' => $this->border_size,
            'border_color' => $this->border_color
        ];
        return $baseFormat::drawShape($shape);
    }
}
