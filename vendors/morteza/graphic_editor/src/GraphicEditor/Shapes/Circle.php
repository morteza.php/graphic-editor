<?php


namespace GraphicEditor\Shapes;


use GraphicEditor\Formats\BaseFormat;

class Circle extends BaseShape
{

    // special attributes
    public $show_center = true;

    public $center_radius = '2px';


    /**
     * @param BaseFormat $baseFormat
     * @return array|mixed
     */
    public function draw(BaseFormat $baseFormat)
    {
        // here based on format we generate the results


        // return dummy result
        $shape = [
            'title' => 'I am supposed to be a Circle',
            'show_center' => $this->show_center,
            'center_radius' => $this->center_radius,
            'color' => $this->color,
            'border_size' => $this->border_size,
            'border_color' => $this->border_color
        ];
        return $baseFormat::drawShape($shape);
    }
}
