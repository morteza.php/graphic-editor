<?php


namespace GraphicEditor\Shapes;


use GraphicEditor\Exceptions\GraphicEditorException;

class BaseShape
{
    // common attributes
    public $color = '#000';

    public $border_size = '2px';

    public $border_color = '#000';

    /**
     * Circle constructor.
     * @param $params
     * @throws GraphicEditorException
     */
    public function __construct($params)
    {
        // set params
        foreach ($params as $paramKey => $paramValue) {
            if (!property_exists($this, $paramKey)) {
                throw new GraphicEditorException("param $paramKey is not supported!");
            }
            $this->$paramKey = $paramValue;
        }
    }
}
