<?php

namespace GraphicEditor;


use GraphicEditor\Exceptions\GraphicEditorException;
use GraphicEditor\Interfaces\GraphicEditorInterface;

class GraphicEditor implements GraphicEditorInterface
{
    private $shapes;

    private $supportedFormats = [
        'points',
        'binary'
    ];

    /**
     * GraphicEditor constructor.
     * @param array $shapes
     */
    public function __construct(array $shapes)
    {
        $this->shapes = $shapes;
    }

    /**
     * @param string $format
     * @return array|mixed
     * @throws GraphicEditorException
     */
    public function draw($format)
    {
        // check the format
        $formatClassName = "GraphicEditor\\Formats\\" . ucfirst(strtoupper($format));
        if (!class_exists($formatClassName)) {
            throw new GraphicEditorException('the format is not supported yet!');
        }
        $formatClass = new $formatClassName;

        $results = [];

        foreach ($this->shapes as $shape) {

            if (empty($shape['type'])) {
                throw new GraphicEditorException('type is not defined!');
            }
            $className = "GraphicEditor\\Shapes\\" . ucfirst(strtolower($shape['type']));

            if (!class_exists($className)) {
                throw new GraphicEditorException('type is not supported yet!');
            }

            $shape = new $className($shape['params']);
            return $shape->draw($formatClass);
        }

        return $results;
    }
}
