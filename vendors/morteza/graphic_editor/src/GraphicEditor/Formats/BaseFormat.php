<?php

namespace GraphicEditor\Formats;


use GraphicEditor\Interfaces\FormatInterface;

class BaseFormat implements FormatInterface
{
    /**
     * @param array $shape
     * @return array|mixed
     */
    public static function drawShape(array $shape)
    {
        // default output format is array
        return $shape;
    }
}