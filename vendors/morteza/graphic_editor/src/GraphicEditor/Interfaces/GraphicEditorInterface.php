<?php

namespace GraphicEditor\Interfaces;


interface GraphicEditorInterface
{
    /**
     * @param string $format
     * @return array|mixed
     */
    public function draw($format);
}
