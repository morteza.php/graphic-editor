<?php

namespace GraphicEditor\Interfaces;


interface FormatInterface
{
    /**
     * @param array $shape
     * @return mixed
     */
    public static function drawShape(array $shape);
}
